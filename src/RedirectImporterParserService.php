<?php

namespace Drupal\redirect_importer;

/**
 * Class RedirectImporterParserService.
 */
class RedirectImporterParserService {

  private $redirect;

  /**
   * Constructs a new RedirectImporterParserService object.
   */
  public function __construct(RedirectService $redirect) {
    $this->redirect = $redirect;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $redirect = $container->get('redirect_importer.create_redirect');
    return new static($redirect);
  }

  /**
   * Determines if there is a query based redirect or direct redirect.
   */
  public function startRedirect($string) {
    $eol = '/\[(.*?)L+(.*?)\]/';
    $lines = preg_split($eol, $string, -1, PREG_SPLIT_NO_EMPTY);

    foreach ($lines as $line) {
      $line = trim(preg_replace('/\s\s+/', '', $line));
      $data = [];
      if ($string) {
        $data = explode("\n", $line);

        if (!empty($data) && count($data) > 1) {
          // When we have conditional queries.
          $this->conditionalRedirect($data[0], $data[1]);
        }
        else {
          // Just a regular redirect.
          $this->normalRedirect($line);
        }
      }
    }

  }

  /**
   * Conditional redirects.
   */
  public function conditionalRedirect($string, $string2) {
    $pattern1 = '/\^(.*?)\Z/';
    $pattern2 = '/\^(.*?)\$/';

    $matches = [];
    preg_match($pattern1, $string, $matches);
    if (empty($matches)) {
      preg_match($pattern1, $string, $matches);
    }
    if (!empty($matches)) {
      $query = $matches[1];
    }
    $from = $this->parseOldPath($string);
    $to = $this->parseDomain($string);

    if ($query) {
      $from = $from . '?' . $query;
    }
    if ($from && $to) {
      if ($to[1]) {
        $this->redirect->createDomainRedirect($from, $to, 301);
      }
      else {
        $this->redirect->createRedirect($from, $to, 301);
      }
    }
  }

  /**
   * Conditional redirects.
   */
  public function normalRedirect($string) {
    $from = $this->parseOldPath($string);
    $to = $this->parseDomain($string);
    if ($from && $to) {
      $this->redirect->createRedirect($from, $to, 301);
    }
  }

  /**
   * Parses the "from" path.
   */
  private function parseOldPath($string) {
    $pattern1 = '/\*(.*?)\$/';
    $pattern2 = '/\^(.*?)\$/';
    $pattern3 = '/\*(.*?)\s/';

    $matches = [];
    preg_match($pattern1, $string, $matches);
    if (empty($matches)) {
      preg_match($pattern2, $string, $matches);
    }
    if (empty($matches)) {
      preg_match($pattern3, $string, $matches);
    }

    if (!empty($matches)) {
      return str_replace("*", '', $matches[1]);
    }
    return NULL;
  }

  /**
   * Parses domain from string.
   */
  private function parseDomain($string) {
    $pattern = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/";
    $matches = [];
    preg_match($pattern, $string, $matches);

    if (!empty($matches)) {
      // We need to know if it's a local domain or external.
      $path = $this->parsePath($matches[0]);
      if ($path) {
        return $path;
      }
    }
    return '';

  }

  /**
   * Parses url to return path or confirms its external.
   */
  private function parsePath($url) {
    $parsed_url = parse_url($url);

    // Local domain.
    if ($parsed_url['host'] == 'www.tracerplus.com') {
      $output = $parsed_url['path'];
      if (isset($parsed_url['query'])) {
        $output .= '?' . $parsed_url['query'];
      }
      \Drupal::logger('my_module2')->debug('<pre>' . print_r($parsed_url, TRUE) . '</pre>');
      return $output;
    }
    else {
      // External domain.
      return $url;
    }
  }

}
