<?php

namespace Drupal\redirect_importer;

use Drupal\redirect\Entity\Redirect;
use Drupal\Component\Utility\UrlHelper;

/**
 * Class RedirectService.
 */
class RedirectService {

  /**
   * Constructs a new CreateRedirectService object.
   */
  public function __construct() {

  }

  /**
   * Creates a redirect, given a from/to.
   */
  public function createRedirect($from, $to, $type = 301, $query = []) {
    $redirect_array = [
      'source' => $from,
      'redirect' => $to,
      'status_code' => $type,
      'language' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
    ];
    if ($redirects = self::redirectExists($redirect_array, $query)) {
      return;
    }
    else {
      $message_type = 'Added';
      $parsed_url = UrlHelper::parse(trim($redirect_array['source']));
      $path = isset($parsed_url['path']) ? $parsed_url['path'] : NULL;

      /** @var \Drupal\redirect\Entity\Redirect $redirect */
      $redirectEntityManager = \Drupal::service('entity.manager')->getStorage('redirect');
      $redirect = $redirectEntityManager->create();
      $redirect->setSource($path, $query);
    }
    // Currently, the Redirect module's setRedirect function assumes
    // all paths are internal. If external, we will use redirect_redirect->set.
    if (parse_url($redirect_array['redirect'], PHP_URL_SCHEME)) {
      $redirect->redirect_redirect->set(0, ['uri' => $redirect_array['redirect']]);
    }
    else {
      $redirect->setRedirect($redirect_array['redirect']);
    }
    $redirect->setStatusCode($redirect_array['status_code']);
    $redirect->setLanguage($redirect_array['language']);
    $redirect->save();
    drupal_set_message(t('@message_type redirect from @source to @redirect', [
      '@message_type' => $message_type,
      '@source' => $redirect_array['source'],
      '@redirect' => $redirect_array['redirect'],
    ]),
    'status');

  }

  /**
   * Check if a redirect already exists for this source path.
   */
  protected static function redirectExists($row, $query = []) {
    // @todo memoize the query.
    $parsed_url = UrlHelper::parse(trim($row['source']));
    $path = isset($parsed_url['path']) ? $parsed_url['path'] : NULL;
    $hash = Redirect::generateHash($path, $query, $row['language']);

    // Search for duplicate.
    $redirects = \Drupal::entityManager()
      ->getStorage('redirect')
      ->loadByProperties(['hash' => $hash]);
    if (!empty($redirects)) {
      return $redirects;
    }
    return FALSE;
  }

}
