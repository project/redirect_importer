<?php

namespace Drupal\redirect_importer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\redirect_importer\RedirectImporterParserService;

/**
 * Class RedirectImporterImportForm.
 */
class RedirectImporterImportForm extends FormBase {

  protected $parser;

  /**
   * Constructs a new DefaultForm object.
   */
  public function __construct(RedirectImporterParserService $parser) {
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('redirect_importer.parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'redirect_importer_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file_paste'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Redirect Text'),
      '#description' => $this->t('Paste the redirects from .htaccess you wish to import.'),
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Parse data.
    $data = $form_state->getValue('file_paste');
    $this->parser->startRedirect($data);
  }

}
